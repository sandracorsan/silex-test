<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

//Request::setTrustedProxies(array('127.0.0.1'));

// Controladores relacionados con la parte de administración del sitio web
//$backend = $app['controllers_factory'];
// Protección extra que asegura que al backend sólo acceden los administradores

/*$backend = $app['controllers_factory'];
$backend->get('/', function () use ($app) {
    return $app['twig']->render('admin.html.twig', array());
})
->bind('admin_index')
;

$app->mount('/admin', $backend);
//$app->mount('/', include 'frontend.php');

$app->before(function (Request $request) use($app) {
    if ($app['security.authorization_checker']->isGranted('ROLE_ADMIN')) {
        return new RedirectResponse($app['url_generator']->generate('homepage'));
    }
});*/

$app->match('/create', function (Request $request) use ($app) {
    $form = $app['form.factory']->createBuilder('form')
        ->add('title', 'text', array(
            'label' => 'Título',
            'required' => true,
            'max_length' => 255,
            'attr' => array(
                'class' => 'form-control',
            )
        ))
        ->add('content', 'textarea', array(
            'label' => 'Contenido',
            'required' => false,
            'max_length' => 2000,
            'attr' => array(
                'class' => 'form-control',
                'rows' => '10',
            )
        ))
        ->getForm();
    return $app['twig']->render('admin.html.twig', array('form' => $form->createView()));
})
->bind('create')
;

$app->get('/', function () use ($app) {
    return $app['twig']->render('index.html.twig', array());
})
->bind('homepage')
;

$app->error(function (\Exception $e, Request $request, $code) use ($app) {
    if ($app['debug']) {
        return;
    }

    // 404.html, or 40x.html, or 4xx.html, or error.html
    $templates = array(
        'errors/'.$code.'.html.twig',
        'errors/'.substr($code, 0, 2).'x.html.twig',
        'errors/'.substr($code, 0, 1).'xx.html.twig',
        'errors/default.html.twig',
    );

    return new Response($app['twig']->resolveTemplate($templates)->render(array('code' => $code)), $code);
});
